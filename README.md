# php-rest-api
This is a step by step PHP 7 & MySQL REST API tutorial, In this tutorial i am going to share with you how to create a PHP 7 CRUD (Create, Read, Update, Delete) RESTful API with MySQL database.

[Create Simple PHP 8 CRUD REST API with MySQL & PHP PDO](https://www.soengsouy.com/2021/04/create-simple-php-8-crud-rest-api-with.html)

## PHP CRUD API
* `GET - http://localhost/api/read.php` Fetch ALL Records
* `GET - localhost/api/single_read.php/?id=2` Fetch Single Record
* `POST - http://localhost/api/create.php` Create Record
* `POST - http://localhost/api/update.php` Update Record
* `DELETE - localhost/api/delete.php` Remove Records
